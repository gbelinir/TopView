﻿using UnityEngine;
using System.Collections;

[RequireComponent(typeof(Rigidbody2D))]
public class Movement : MonoBehaviour {

    public float speed;
    private Rigidbody2D m_rigidbody;

    void Start()
    {
        m_rigidbody = GetComponent<Rigidbody2D>();
    }

    public void Move(Transform target)
    {
        Vector2 targetDirection = target.position - transform.position;
        targetDirection = targetDirection.normalized;

        Move(targetDirection);
    }

    public void Move(Vector2 direction)
    {
        direction = direction.normalized;

        m_rigidbody.velocity = direction * speed;
    }

    public void MovementStop()
    {
        m_rigidbody.velocity = Vector2.zero;
    }
}
