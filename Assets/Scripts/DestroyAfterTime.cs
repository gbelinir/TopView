﻿using UnityEngine;
using System.Collections;

public class DestroyAfterTime : MonoBehaviour {

    public float time;

    public void TimeofDeath()
    {
        Invoke("Death", time);
    }

    private void Death()
    {
        Destroy(gameObject);
    }
}
