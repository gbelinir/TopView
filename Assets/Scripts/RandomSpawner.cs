﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class RandomSpawner : MonoBehaviour
{
    public float respawnRate;
    public List<GameObject> EnemyPrefabs;
    public List<Transform> RespawnList;

    private Vector2 m_rdmPos;
    
    void Start()
    {
        InvokeRepeating("SpawnOrc", 1, respawnRate);
    }

    private void SpawnOrc()
    {
        int rdmPoint = Random.Range(0, RespawnList.Count);

        Vector2 pos = RespawnList[rdmPoint].position;

        GameObject enemy = EnemyPrefabs[Random.Range(0, EnemyPrefabs.Count)];

        Instantiate(enemy, pos, Quaternion.identity);
    }

}
