﻿using UnityEngine;
using System.Collections;
using System;

public class BaseEnemy : MonoBehaviour
{
    public Animator anim;

    private Armor m_armor;
    private Action m_currentState;
    private Movement m_movement;
    private Vector2 m_initialPos;
    private bool m_dead;
    private Transform m_princessTransform;

    void Start()
    {
        m_princessTransform = GameObject.FindGameObjectWithTag("Chest").transform;

        m_movement = GetComponent<Movement>();

        m_currentState = GetMoneyState;

        m_initialPos = transform.position;

        m_armor = GetComponent<Armor>();
    }

    void FixedUpdate()
    {
        if (m_dead)
        {
            m_movement.MovementStop();
            return;
        }

        if (m_currentState != null)
            m_currentState.Invoke();
    }

    void Update()
    {
        if (m_armor.currentHp <= 0)
        {
            m_princessTransform.parent = null;
        }
    }

    public void GetMoneyState()
    {
        m_movement.Move(m_princessTransform);
    }

    public void RunState()
    {
        m_movement.Move(m_initialPos);
    }

    void OnTriggerEnter2D(Collider2D hit)
    {
        if (hit.tag == "Chest")
        {
            m_princessTransform.parent = transform;

            m_currentState = RunState;
        }
    }

    public void TakeDamageLocal()
    {
        anim.SetTrigger("TakeHit");
        anim.SetInteger("Life", (int)m_armor.currentHp);
    }

    //Called by a SendMessage in armor script
    public void DeathLocal()
    {
        m_dead = true;

        GetComponent<DestroyAfterTime>().TimeofDeath();
    }
}
