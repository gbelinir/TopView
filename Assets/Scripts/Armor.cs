﻿using UnityEngine;
using System.Collections;

public class Armor : MonoBehaviour
{

    public float totalHp;
    public float currentHp;
    public float cooldownTime;
    public string enemyTag;

    private bool m_onCooldown;
    private float m_time;

    void Start()
    {
        currentHp = totalHp;
    }

    void Update()
    {
        if (m_onCooldown)
        {
            m_time += Time.deltaTime;

            if (m_time > cooldownTime)
            {
                m_onCooldown = false;
                m_time = 0;
            }
        }
    }

    void OnTriggerEnter2D(Collider2D hit)
    {
        if (hit.transform.root.tag == enemyTag)
        {
            if (m_onCooldown || hit.tag != "DamageArea") return;

            m_onCooldown = true;

            Attack attackComponent = hit.transform.root.GetComponent<Attack>();

            if (attackComponent != null)
                TakeDamage(attackComponent);
        }
    }

    private void TakeDamage(Attack attack)
    {
        currentHp -= attack.damage;

        if (currentHp <= 0)
        {
            Death();
        }

        SendMessage("TakeDamageLocal", SendMessageOptions.DontRequireReceiver);
    }

    private void Death()
    {
        SendMessage("DeathLocal", SendMessageOptions.DontRequireReceiver);
    }
}
