﻿using UnityEngine;
using System.Collections;

public class Flip : MonoBehaviour {

    private bool m_facingRight;
    private Rigidbody2D m_rigidBody;

    void Start()
    {
        m_rigidBody = GetComponent<Rigidbody2D>();
    }

    void Update()
    {
        if (m_facingRight && m_rigidBody.velocity.x > 0)
            FlipSprite();
        if (!m_facingRight && m_rigidBody.velocity.x < 0)
            FlipSprite();
    }

    void FlipSprite()
    {
        m_facingRight = !m_facingRight;

        Vector3 theScale = transform.localScale;
        theScale.x *= -1;
        transform.localScale = theScale;
    }
}
